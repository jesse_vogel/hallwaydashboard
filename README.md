# Hallway Visualizations 

This projected was created for the 2018 Amplify Hackathon. The goal of the project was to create a slideshow of data visualization dashboards to replace the unpopular Amplify video that lived in the front hallway, (RIP, but not really.) 

Our team includes:

- Eric Connelly
- Annie Cypar
- Luda Janda 
- Greg Nofi
- Jesse Vogel
- (and Andy Worhol)

## Project Goal
Apart from retiring the video that has been playing on repeat, (continuously since the beginning of time) we wanted to create a space where Amplify employees could see the impact that our products are creating. Data visualization can help give context to our work and remind us of why we're here doing what we do.